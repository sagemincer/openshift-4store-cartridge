To make these files into a working cartridge, create an app using the Cartridge Development Kit using this code as a base.

   rhc app create fscdk http://cdk-claytondev.rhcloud.com/ --from-code https://gitlab.com/sagemincer/openshift-4store-cartridge.git
